"""Test bot commands."""
import unittest
from unittest import mock

from freezegun import freeze_time

from irc_bot import irc_commands


class TestShuffledMembers(unittest.TestCase):
    """Test shuffled_members."""

    def test_shuffled_members(self):
        """Test shuffled_members."""
        cases = (
            ('2010-01-01', 'foo bar baz', None, 'baz, foo, bar'),
            ('2010-01-04', 'foo bar baz', None, 'foo, baz, bar'),
            ('2010-01-01', 'foo bar baz', 'foo', 'baz, bar'),
            ('2010-01-01', 'bar baz', 'foo', 'baz, bar'),

        )
        for time, members, remove, expected in cases:
            with freeze_time(time), mock.patch('irc_bot.irc_commands.TEAM_MEMBERS', members):
                for _ in range(10):
                    self.assertEqual(irc_commands.shuffled_members(remove=remove), expected)


class TestCoffeeTime(unittest.TestCase):
    """Test coffeetime command."""
    mock_standups = {
        '0': ['Mon', 'http://test.url/mon'],
        '2': ['Wed', 'http://test.url/wed']
    }

    @mock.patch('irc_bot.irc_commands.STANDUPS', mock_standups)
    @mock.patch('irc_bot.irc_commands.shuffled_members', mock.Mock(return_value='foo, bar, baz'))
    def test_command(self):
        """Test coffeetime command."""
        mock_response = mock.Mock()
        mock_event = mock.Mock()
        command_object = irc_commands.CoffeeTime()

        data = [
            [1, '2022-11-28', '- http://test.url/mon /cc foo, bar, baz'],
            [2, '2022-11-29', '/cc foo, bar, baz'],
            [3, '2022-12-07', '- http://test.url/wed /cc foo, bar, baz']
        ]

        for calls, date, expected in data:
            with freeze_time(date):
                command_object.run(mock_response, mock_event, [])
                self.assertEqual(mock_response.privmsg.call_count, calls)
                mock_response.privmsg.assert_called_with(
                    mock_event.target,
                    f"🙋 ☕️☕️☕️⏰ Virtual Coffee Time ☕️☕️☕️⏰ {expected}"
                )
